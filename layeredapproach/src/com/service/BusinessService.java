package com.service;

public class BusinessService {

	public  int sumAllValues(int[ ]  array) {
		//logic 
		int tempVar = 0;
		for (int i = 0; i < array.length; i++) {
			tempVar += array[i];
		}
		return tempVar;
	}
	
	public int findTheBiggestNumber(int[ ]  array)
	{
		int dummyVar = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] > dummyVar) {			//logic 
				dummyVar = array[i];
			}
		}
		return dummyVar;
	}

	public boolean searchForNumber (int[ ]  array,int search) {
		int dummyVar = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] ==search) {			//logic 
				dummyVar = array[i];
			}
		}
	    if(dummyVar > 0 ) {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	
}
