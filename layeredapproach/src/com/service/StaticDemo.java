package com.service;

public class StaticDemo {

	public static int var = 10;

	private float floatVar = 22.34f; // instance variable

	public static void display() { // created only once == one copy

		System.out.println("This from static method" + var);

	}

}
