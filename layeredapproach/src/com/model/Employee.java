package com.model;

/**
 * 
 * @author jason.d
 *
 */
// variable are private and public getter and setter == POJO Plain Old Java
// Object == model ==bean
public class Employee {

	// oop the variable should be private

	private int empNo;
	private String empName;
	private float salary;
	private int age;

	public Employee() {
		super();
	}

	
	
	public Employee(int empNo) {
		super();
		this.empNo = empNo;
	}



	public Employee(int empNo, String empName, float salary, int age) {
		super();
		//logic == at the time of creation
		System.out.println("4 parameter constructor is called");
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
		this.age = age;
	}



	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	// public methods to access

}
