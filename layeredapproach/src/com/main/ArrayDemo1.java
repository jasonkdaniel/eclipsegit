package com.main;

import com.service.BusinessService;

public class ArrayDemo1 {
	public static void main(String[] args) {

		int intVar[] = { 1, 2, 3, 4, 5 };

		BusinessService businessService = new BusinessService();
		int ans = businessService.sumAllValues(intVar);
		System.out.println(ans);

		int big = businessService.findTheBiggestNumber(intVar);
		System.out.println("Biggest value : " + big);
		int whatValToSearch = 7;
		boolean val = businessService.searchForNumber(intVar, whatValToSearch);
		if (val == true) {
			System.out.println("Number is present" + whatValToSearch);
		} else {
			System.out.println("Number not present" + whatValToSearch);
		}

	}

}
