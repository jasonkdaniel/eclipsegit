package com.main;

import com.model.Employee;

public class MainApp {

	public static void main(String[] args) {
		Employee employee = new Employee(10, "World", 1234f, 27); // access because the class is public
		
		System.out.println("Employee number : " + employee.getEmpNo());   //10
		System.out.println("Employee name   : " + employee.getEmpName()); //World
		System.out.println("Employee age    : " + employee.getAge());     //27
		
		
		employee.setEmpName("Updated name");
		employee.setAge(28);
		System.out.println("Employee number : " + employee.getEmpNo());    //10
		System.out.println("Employee name   : " + employee.getEmpName());  // Updated name 
		System.out.println("Employee age    : " + employee.getAge());      //28 
	}

}
