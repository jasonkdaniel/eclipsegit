package com.main;

import com.model.Employee;
import com.service.EmployeeService;

public class EmployeeArrayDemo {

	public static void main(String[] args) {
		Employee employee1 = new Employee(10, "Ten", 1010.10f, 22);
		Employee employee2 = new Employee(20, "Twenty", 2020.20f, 23);
		Employee employee3 = new Employee(30, "Thirty", 3030.30f, 24);
		Employee employee4 = new Employee(40, "Fourty", 4040.40f, 23);

		int[] intVar = { 1, 2, 3, 4 }; // 1

		int[] intArray = new int[4]; // created in heap
		// collection of employees
		Employee[] employees = new Employee[4];
		employees[0] = employee1;
		employees[1] = employee2;
		employees[2] = employee3;
		employees[3] = employee4;

		// search by empid = ??
		EmployeeService employeeService = new EmployeeService();
		Employee employee = employeeService.searchByEmployeeID(employees, 50);
		// avoid null pointer exception
		if (employee != null) {
			System.out.println("Employee number  : " + employee.getEmpNo());
			System.out.println("Employee name    : " + employee.getEmpName());
		} else {
			System.out.println("Sorry no such record ");
		}

		System.out.println("search by age ");
		// search by age
		Employee[] employee5 = employeeService.searchByEmployeeAge(employees, 23);
		if (employee5.length > 0) {
			for (int i = 0; i < employee5.length; i++) {
				if (employee5[i] != null) {
					System.out.println("Employee name " + employee5[i].getEmpName());
				}
			}
		} else {
			System.out.println("No employee with that age ");
		}

	}

}
