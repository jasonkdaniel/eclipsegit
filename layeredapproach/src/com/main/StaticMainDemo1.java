package com.main;

import com.service.StaticDemo;

public class StaticMainDemo1 {

	int a = 22; 
	public static void main(String[] args) {  // method signature 
		
		System.out.println(StaticDemo.var);
		
		StaticDemo.display();
		
		StaticDemo.var = 23;
		System.out.println("After change : " + StaticDemo.var);

	}

}
