package com.main;

import java.util.Scanner;

import javax.swing.plaf.synth.SynthScrollBarUI;

import com.model.Employee;

public class CommandLineDemo {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);// console
		System.out.println("Enter employee number :");
		int no = scanner.nextInt(); // dynamic

		System.out.println("Enter employee name :");
		String name = scanner.next();

		System.out.println("Enter salary :");
		float sal = scanner.nextFloat();

		System.out.println("Enter age");
		int age = scanner.nextInt(); // Dynamic == runtime

		Employee employee = new Employee();
		employee.setEmpNo(no);
		employee.setEmpName(name);
		employee.setSalary(sal);
		employee.setAge(age);

		System.out.println("Employee number " + employee.getEmpNo());
		System.out.println("Employee name   " + employee.getEmpName());
		System.out.println("Employee salary " + employee.getSalary());
		System.out.println("Employee age " + employee.getAge());
	}

}
